#!/usr/bin/env python

"""
A hyper parameter optimizer for the Nao predictor.
"""
from hyperopt import fmin, tpe, hp, Trials, STATUS_OK
from time import strftime, time
import nao_predictor

__author__ = "Roman Uglovskij"
__copyright__ = "Copyright 2018, Roman Uglovskij"
__credits__ = "Roman Uglovskij"
__license__ = "BSD 2.0"
__version__ = "1.0.1"
__maintainer__ = "Roman Uglovskij"
__email__ = "roman.uglovskij@informatik.hu-berlin.de"
__status__ = "Release to web (RTW)"

def objective(argv):
	"""The objective function. Returns the total_error from nao_predictor's main function and a
	status value about the test success.

	argv: A dict of parameter values. Must be of the form {'neurons' : <integer>,
	'samples' : <integer>, 'multiplier' : <float>, 'alpha' : <float>, 'm' : <float>,
	'seconds' : <float>, 'epochs' : <integer> } 
	"""
	return {
		'loss': nao_predictor.main(argv),
		'status': STATUS_OK,
	}

def saveResults(argv, error, file_name):
	"""Saves the tab-separated results within the file specified in file_name.

	argv: A dict of parameter values. Must be of the form {'neurons' : <integer>,
	'samples' : <integer>, 'multiplier' : <float>, 'alpha' : <float>, 'm' : <float>,
	'seconds' : <float>, 'epochs' : <integer> } 
	
	error: The total_error calculated for the set of argv parameters. Must be a float.
	
	file_name: The name of the file to save the results in. Must be a string.
	"""
	#TODO: Make this code more compact
	outputFile = open(file_name, 'a')	
	outputFile.write(str(argv['neurons']))
	outputFile.write("\t")
	outputFile.write(str(argv['samples']))
	outputFile.write("\t")
	outputFile.write(str(argv['multiplier']))
	outputFile.write("\t")
	outputFile.write(str(argv['alpha']))
	outputFile.write("\t")
	outputFile.write(str(argv['m']))
	outputFile.write("\t")
	outputFile.write(str(argv['seconds']))
	outputFile.write("\t")
	outputFile.write(str(argv['epochs']))
	outputFile.write("\t")
	outputFile.write(str(error))
	outputFile.write("\n")
	outputFile.close()

def main():
	"""
	Main function. Creates 100 value tuples with their corresponding errors 4 times saved in 4
	result files. Each tuple is evaluated 10 times before a result is produced.
	"""
	#TODO: Make these variables passable via function call
	training_sessions = 1
	test_values = 10
	evaluations = 2

	hpspace = {
		'neurons' 	 : hp.randint('neurons', 96),
		'samples' 	 : hp.randint('samples', 50),
		'multiplier' : hp.uniform('multiplier', 1e-10, 1e-1),
		'alpha'   	 : hp.uniform('alpha', 0.01, 1.0),
		'm'    		 : hp.uniform('m', 0.01, 1.0),
		'seconds' 	 : hp.uniform('seconds', 0.01, 3.0),
		'epochs'  	 : hp.randint('epochs', 20),
	}
	
	for i in range(training_sessions):

		current_time = strftime("%Y_%m_%d_%H%M")

		session = i + 1

		print("=======================================================================================")
		print("Beginning training session number {0} for {1} ({2}):".format(i+1, session, strftime("%H:%M:%S")))
		print("=======================================================================================")

		file_name = "results/results_" + strftime("%y.%m.%d") + "_" + str(session)

		# Create the file and add the header
		outputFile = open(file_name, 'w+')
		outputFile.write("neurons\tsamples\tmultiplier\talpha\tm\tseconds\tepochs\terror\n")
		outputFile.close()

		for j in range(test_values):

			test_start = time()
		
			print("Finding value tuple number {0}/{1} ({2}):".format(j+1, test_values, strftime("%H:%M:%S")))

			trials = Trials()
			best = fmin(objective, space=hpspace, algo=tpe.suggest, max_evals=evaluations, trials=trials)

			test_end = time()

			print("Completed in {0} seconds".format(test_end - test_start))
			print("---------------------------------------------------------------------------------------")

			error = min(trials.losses())

			print("Result: {0} with an error of {1}".format(best, error))
			print("---------------------------------------------------------------------------------------")

			saveResults(best, error, file_name)

if __name__ == "__main__":
	main()
	
