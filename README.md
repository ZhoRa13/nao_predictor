# Nao Predictor

The Nao Predictor is a simple predictive model for the left arm joint angles of a Nao robot. The
system generates 6 goals, each a vector of 4 left arm joint angles (Shoulder Pitch, Shoulder Roll,
Elbow Yaw and Elbow Roll) for the robot to try and reach. The system then applies a portion of the
movement predicted by the neural network and evaluates the new distance to the goal. Based on that,
the neural network is updated and a new prediction is made. The Optimizer is used for hyper parameter
optimization of the system. It saves the best tuples of parameter values in addition to the error
they produce within a separate text file within the _results/_ folder.

![The model](/models/predictive_model.png)

## Getting started

These instructions will get you a copy of the project up and running on your local machine for
development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* [Keras](http://www.keras.io/) - The framework used for the neural network
* [Hyperopt](http://github.com/hyperopt/hyperopt) - The hyper parameter optimizer
* [NumPy](http://www.numpy.org/) - Python library for vector math
* [PyPlot](https://matplotlib.org/api/pyplot_api.html) - Python library for plotting the results
* [H5Py](https://www.h5py.org/) - Python library for saving the neural network as a .h5py file

### Installing

Once all the dependancies are met, simply clone the repository and run it. No other installation
steps are required.

## Deployment

Run the predictor via the basic console command

```
python nao_predictor.py
```

Run the optimizer in the same manner. It is advised to comment out line 279 of the predictor
(plt.show()) to avoid seeing the results after every optimizer iteration.

```
python optimizer.py
```

## Author

* **Roman Uglovskij**

## License

This project is licensed under the MIT License (MIT).

## Acknowledgments

* [Prof. Dr. Verena V. Hafner](https://adapt.informatik.hu-berlin.de/vvh/index.html)
* [Oswald Berthold](https://www2.informatik.hu-berlin.de/~oberthol/html/Public%20Index.html)
* [Dr. Guido Schillaci](https://adapt.informatik.hu-berlin.de/schillaci/)
* Keras development team
	* Copyright (c) 2015 - 2018, François Chollet.
	* Copyright (c) 2015 - 2018, Google, Inc.
	* Copyright (c) 2017 - 2018, Microsoft, Inc.
	* Copyright (c) 2015 - 2018, all other contributors.
	* [Homepage](http://www.keras.io/)
	* MIT License (MIT)
* Hyperopt
	* Copyright (c) 2013, James Bergstra
	* [Homepage](http://github.com/hyperopt/hyperopt)
	* BSD 2.0 License
* h5py
    * Copyright (c) 2008-2013 Andrew Collette and contributors
    * [Homepage](http://www.h5py.org)
    * BSD 2.0 License
