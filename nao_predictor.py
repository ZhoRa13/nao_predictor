#!/usr/bin/env python

#TODO: Add default values for functions; take care of other TODOs

"""
A predictive model for the Nao robot.
"""
import numpy as np						# For simpler vector math
from random import uniform				# To get random angle values			
from time import time					# To set and measure the run duration

import keras							# The ANN
from keras.models import Sequential		# The Sequential model
from keras.layers import Dense			# To add layers
from keras import backend as K			# To release GPU memory after a run

import h5py								# To save the model
from keras.models import load_model		# To load a saved model
from keras.utils import plot_model		# To visualize the model
from keras import optimizers			# To add different optimizers

import matplotlib.pyplot as plt			# To plot the results

__author__ = "Roman Uglovskij"
__copyright__ = "Copyright 2018, Roman Uglovskij"
__credits__ = "Roman Uglovskij"
__license__ = "BSD 2.0"
__version__ = "1.0.1"
__maintainer__ = "Roman Uglovskij"
__email__ = "roman.uglovskij@informatik.hu-berlin.de"
__status__ = "Release to web (RTW)"

def createModel(neurons, samples, multiplier):
	"""Returns an artificial neural network with eight input neurons, one hidden layer with a
	specified amount of neurons and four output neurons. 

	neurons: Amount of neurons in the hidden layer. Must be an integer greater than 4.

	samples: Amount of prior knowledge dummy samples. Must be an integer.

	mutliplier: A value by which the dummy sample's labels are mutliplied to stay small. Should
	be a very small float value like 1e-3.
	"""
	model = Sequential()
	activation_func = 'relu'
	loss_func = 'logcosh'
	optimizer_func = 'adagrad'
	model.add(Dense(neurons, input_shape=(8,), activation=activation_func))
	model.add(Dense(4, activation='linear', name='output_layer'))
	model.compile(loss=loss_func, optimizer=optimizer_func, metrics=['accuracy'])
	model = initModel(model, samples, multiplier)
	return model

def initModel(model, samples, multiplier):
	"""Returns pseudo-trained model with a set amount of random dummy samples. 

	model: A Keras sequential model.

	samples: Amount of prior knowledge dummy samples. Must be an integer.

	mutliplier: A value by which the dummy sample's labels are mutliplied to stay small. Should
	be a very small float value like 1e-3.
	"""
	for i in range(samples):
		g = np.array([uniform(-2.0856, 2.0856), uniform(-0.3141, 1.3264), uniform(-2.0856, 2.0856), 
						uniform(-1.5446, -0.0349)]) * multiplier
		e = np.array([uniform(-2.0856, 2.0856), uniform(-0.3141, 1.3264), uniform(-2.0856, 2.0856),
						uniform(-1.5446, -0.0349)]) * multiplier
		x_train = np.append(g, e).reshape(1, 8)
		y_train = np.array([uniform(-2.0856, 2.0856), uniform(-0.3141, 1.3264),
							uniform(-2.0856, 2.0856), uniform(-1.5446, -0.0349)]).reshape(1, 4) * multiplier
		model.fit(x_train, y_train, batch_size=1, epochs=1, verbose=0)
	return model

def saveModel(model, model_name):
	"""Saves the Keras model into the the models/ sub-folder as a <model_name>.h5 file. Also saves
	the model's	weights separately in the <model_name>_weights.h5 and the visual representation as a
	<model_name>.png file.

	model: A Keras model.

	model_name: The name of the model to be used. Must be a string.

	Note: Currently not used.
	"""
	model.save('models/' + model_name + '.h5')
	model.save_weights('models/'+ model_name + '_weights.h5', overwrite=True)
	plot_model(model, to_file='models/' + model_name + '.png')
	print("Model {0} saved!".format(model_name))
    
def loadModel(model_name):
	"""Loads a Keras model from <model_name>.h5 file and the weights from <model_name>_weights.h5
	from within the models/ subfolder.

	model_name: The name of the saved model. Must be a string.

	Note: Currently not used.
	"""
	model = load_model('models/' + model_name + '.h5')
	model.load_weights('models/' + model_name + '_weights.h5')
	optimizer_func = 'adagrad'
	loss_func = 'logcosh'
	model.compile(optimizer=optimizer_func, loss=loss_func)
	return model

def updateModel(model, goal, error_pred, target, epochs):
	"""Updates the model.

	model: The Keras model.
	
	goal: The target goal. Must be a 4x1 vector. 
	
	error_pred: Prediction error. Must be a 4x1 vector.
	
	target: The prediction. Must be a 4x1 vector.
	
	epochs: The amount of epochs to fit the model. Must be an integer.
	"""
	x_train = np.append(goal, error_pred).reshape(1, 8)
	y_train = target.reshape(1, 4)

	model.fit(x_train, y_train, batch_size=1, epochs=epochs, verbose=0)

	return model

def predict(model, goal, error_pred):
	"""Generate a prediction based on the input of the model.

	model: The Keras model.
	
	goal: The target goal. Must be a 4x1 vector. 
	
	error_pred: Prediction error. Must be a 4x1 vector.
	"""
	x_pred = np.append(goal, error_pred).reshape(1, 8)

	prediction = model.predict(x_pred, batch_size=1)

	return prediction

def plotResults(array, goal, goals_total, goal_num):
	"""Plot the results. Creates a subplot for each goal and a curve for each joint position.
	Also plots end points for the goal position.

	array:
	
	goal: The target goal. Must be a 4x1 vector. 
	
	goals_total: The total number of goals. Must be an integer.
	
	goal_num: The number of the current goal. Must be an integer not greater than goals_total.
	"""
	plt.subplot(2, 3, goal_num)
	plt.plot(array[0], label="Shoulder Pitch")
	plt.plot(array[1], label="Shoulder Roll")
	plt.plot(array[2], label="Elbow Yaw")
	plt.plot(array[3], label="Elbow Roll")

	plt.scatter(len(array[0]), goal.item(0))
	plt.scatter(len(array[0]), goal.item(1))
	plt.scatter(len(array[0]), goal.item(2))
	plt.scatter(len(array[0]), goal.item(3))

	plt.xlabel('Iteration')
	plt.ylabel('Angles')

	if goal_num == 2:
		plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)

def printStatus(argv):
	"""Prints all model values to the console for minor feedback.
	
	argv: A dict of parameter values. Must be of the form {'neurons' : <integer>,
	'samples' : <integer>, 'multiplier' : <float>, 'alpha' : <float>, 'm' : <float>,
	'seconds' : <float>, 'epochs' : <integer> } 
	
	"""
	print("=======================================================================================")
	print("Creating model with {0} neurons in the hidden layer.".format(argv['neurons']))
	print("Creating model with {0} samples in the prior knowledge.".format(argv['samples']))
	print("Multiplying the random prior kowledge with {0}.".format(argv['multiplier']))
	print("Setting learning rate to {0}.".format(argv['alpha']))
	print("Setting joint movement speed to {0}.".format(argv['m']))
	print("Spending {0} seconds per iteration.".format(argv['seconds']))
	print("Setting fit epochs to {0}.".format(argv['epochs']))
	print("=======================================================================================")

def main(argv=None):
	"""Main function. Generates goals and lets the model reach them. Calls all the plotResults
	function to visualize the model's progress. Returns the total error over all goals.
	
	argv: A dict of parameter values. Must be of the form {'neurons' : <integer>,
	'samples' : <integer>, 'multiplier' : <float>, 'alpha' : <float>, 'm' : <float>,
	'seconds' : <float>, 'epochs' : <integer> } 
	"""
	#TODO: Make the function calls for plotResults and printStatus be set externally.
	if argv == None:
		# Default values if called w/o parameters
		argv = {'neurons' : 25, 'samples' : 20, 'multiplier' : 1e-3, 'alpha' : 0.5, 'm' : 0.5,
				'seconds' : 0.5, 'epochs' : 1 }

	# Get the separate parameter values from the vector
	neurons = argv['neurons'] + 4	# 4 neurons should be the minimum. Hyperopt starts from 0
	samples = argv['samples']
	multiplier = argv['multiplier']
	alpha = argv['alpha']
	m = argv['m']
	seconds = argv['seconds']
	fit_epochs = argv['epochs']

	# Console output
	printStatus(argv)

	# Create and initialize model
	model = createModel(neurons, samples, multiplier)
    
	# Start state for all joints set to 0 rad
	state_start = np.array([0.0, 0.0, 0.0, 0.0])

	# At the start, the current state is the start state
	state_new = state_start

	#TODO: Make this number variable or passed through the function call
	# Number of goals to try and achieve
	goals = 6

	# The sum of all errors over all goals
	total_error = 0

	# Try for all different goals
	for x in range(goals):

		# Generate a random goal we want to achieve within the joint constraints
		goal = np.array([uniform(-2.0856, 2.0856), uniform(-0.3141, 1.3264),
						uniform(-2.0856, 2.0856), uniform(-1.5446, -0.0349)])

		# Duration for runs in seconds
		duration = seconds
		t_end = time() + duration

		# An empty list for all the coordinate values for plotting
		state_plot = [ [], [], [], [] ]

		# Iterate through the model for the set amount of time
		while time() < t_end:

			state_current = state_new

			state_plot[0].append(state_current.item(0))
			state_plot[1].append(state_current.item(1))
			state_plot[2].append(state_current.item(2))
			state_plot[3].append(state_current.item(3))

			# The prediction error
			error_pred = state_current - goal

			# Make a prediction
			prediction = predict(model, goal, error_pred)

			# Apply the movement
			movement = prediction * m
			state_new = state_start + movement

			# Error for updating the model
			error_fit = state_new - goal

			target = prediction - alpha * error_fit
        
			# Update the model using the new label
			model = updateModel(model, goal, error_pred, target, fit_epochs)

		plotResults(state_plot, goal, goals, x+1)

		error_vector = state_new - goal

		total_error += np.sum(np.absolute(error_vector))

	# Make this line callable or uncallable via an external variable
	plt.show()

	# Release the GPU memory
	K.clear_session()

	return float(total_error)

if __name__ == "__main__":

	main()

